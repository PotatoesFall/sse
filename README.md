# sse

_"A little copy-paste is better than a little dependency"_

this library is so small, you're probably better off just copying it rather than importing it as a dependency!

[example](example/example.go)

Note: this library does not support the `event: `, `id: ` or `retry: ` fields (yet)


