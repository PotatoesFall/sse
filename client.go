package sse

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
)

var (
	dataPrefix    = []byte("data: ")
	commentPrefix = byte(':')
)

// ScanData expects a SSE http response body,
// reads the `data:` field in each message,
// and sends them on the returned channel.
//
// It supports comments and multiline messages, but does not do any special
// handling for unexpected formats.
//
// It only uses the `data` field, and ignores `event`, `id` and `retry`.
// It ignores messages without data.
//
// If the error channel is not drained, errors will be dropped.
//
// See https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events#fields
func ScanData(ctx context.Context, r io.ReadCloser) (chan []byte, chan error) {
	ch := make(chan []byte)
	errCh := make(chan error, 1)

	go func() {
		defer close(ch)
		defer close(errCh)
		defer r.Close()

		scanner := bufio.NewScanner(r)

		// we loop through lines. In SSE, messages are separated by double newlines.
		// however there can be multiple `data` lines which should be joined by newlines.
		var currentMessage []byte
		for scanner.Scan() {
			line := scanner.Bytes()
			if len(line) == 0 {
				// empty line is equivalent to a double newline, which is the message delimiter in SSE
				// this means the current message (if not empty) is finished and ready to be sent.

				if len(currentMessage) == 0 {
					continue
				}

				select {
				case ch <- currentMessage:
					// reset message
					currentMessage = nil
					continue

				case <-ctx.Done():
					return
				}
			}

			if line[0] == commentPrefix { // comment, skip
				continue
			}

			if !bytes.HasPrefix(line, dataPrefix) {
				// all lines should either be empty, comments, or data

				select {
				case errCh <- fmt.Errorf("received line with unexpected prefix: %q", string(line)):
				default:
				}
				continue
			}

			// multi-line messages should be joined by newlines
			if len(currentMessage) != 0 {
				currentMessage = append(currentMessage, '\n')
			}
			currentMessage = append(currentMessage, line[len(dataPrefix):]...)
		}
	}()

	return ch, errCh
}
