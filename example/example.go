package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"codeberg.org/PotatoesFall/sse"
)

func server() {
	panic(http.ListenAndServe(":8080", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		rw := sse.Response(ctx, w, 15*time.Second)

		for ctx.Err() == nil {
			time.Sleep(time.Second)
			err := rw.Send([]byte("hello"))
			if err != nil {
				fmt.Println("SERVER ERROR", err)
				return
			}
		}
	})))
}

func client() {
	resp, err := http.DefaultClient.Get("http://localhost:8080")
	if err != nil {
		panic(err)
	}

	ch, errCh := sse.ScanData(context.Background(), resp.Body)

	for {
		select {
		case msg := <-ch:
			fmt.Println(string(msg))
		case err := <-errCh:
			fmt.Println("CLIENT ERROR", err.Error())
		}
	}
}

func main() {
	go server()
	client()
}
