package sse

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net/http"
	"sync"
	"time"
)

// ResponseWriter wraps a HTTP response and can write SSE messages.
type ResponseWriter struct {
	mu       sync.Mutex
	w        http.ResponseWriter
	interval time.Duration
	lastSend time.Time
	stopped  bool
}

// ErrStopped is returned by `ResponseWriter.Send` when the request (or ctx) has been cancelled.
var ErrStopped = errors.New("sse response writer already stopped")

func (w *ResponseWriter) Send(msg []byte) error {
	w.mu.Lock()
	defer w.mu.Unlock()

	if w.stopped {
		return ErrStopped
	}

	// write the event and flush to connection
	for _, line := range bytes.Split(msg, []byte{'\n'}) {
		_, err := fmt.Fprintf(w.w, "data: %s\n", line)
		if err != nil {
			return err
		}
	}
	_, err := fmt.Fprint(w.w, "\n")
	if err != nil {
		return err
	}
	tryFlush(w.w)

	w.lastSend = time.Now()

	return nil
}

// Response writes to a HTTP SSE response.
// w MUST be a http.Flusher.
// once the request is dropped or ctx is cancelled, WriteResponse will return ErrStopped
// on any calls to Send().
func Response(ctx context.Context, w http.ResponseWriter, interval time.Duration) *ResponseWriter {
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-store")
	w.Header().Set("X-Accel-Buffering", "no")
	w.WriteHeader(http.StatusOK)

	// initial comment to ensure the headers are sent
	_, _ = fmt.Fprint(w, ":comment\n\n")
	tryFlush(w)

	rw := ResponseWriter{
		w:        w,
		interval: interval,
		lastSend: time.Now(),
	}

	go func() {
		defer func() {
			rw.mu.Lock()
			rw.stopped = true
			rw.mu.Unlock()
		}()
		timer := time.NewTimer(interval)
		defer timer.Stop()

		for {
			select {
			case <-ctx.Done():
				return

			// keepalive - some proxies might drop the connection if nothing is being sent.
			// Comments can be sent as a keepalive: https://html.spec.whatwg.org/multipage/server-sent-events.html#authoring-notes
			// if it has been longer than interval, send a comment to keep the connection alive
			case <-timer.C:
				rw.mu.Lock()
				if time.Since(rw.lastSend) < interval {
					timer.Reset(time.Until(rw.lastSend.Add(interval)))
				} else {
					_, _ = fmt.Fprint(w, ":keepalive\n\n")
					tryFlush(w)
					timer.Reset(interval)
				}
				rw.mu.Unlock()
			}
		}
	}()

	return &rw
}

// tryFlush attempts to flush the response writer if it is a http.Flusher.
func tryFlush(w http.ResponseWriter) {
	flusher, ok := w.(http.Flusher)
	if ok {
		flusher.Flush()
	}
}
