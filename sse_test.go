package sse_test

import (
	"bytes"
	"context"
	"errors"
	"io"
	"net/http"
	"slices"
	"sync"
	"testing"
	"time"

	"codeberg.org/PotatoesFall/sse"
)

var expected = []byte(`:comment

data: hello
data: world

:keepalive

:keepalive

data: hello
data: world

:keepalive

:keepalive

data: hello
data: world

`)

func TestSSE(t *testing.T) {
	go http.ListenAndServe(":8080", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		rw := sse.Response(ctx, w, 50*time.Millisecond)

		for ctx.Err() == nil {
			err := rw.Send([]byte("hello\nworld"))
			if err != nil {
				t.Error("server error", err)
				return
			}
			time.Sleep(101 * time.Millisecond)
		}
	}))

	t.Run("test raw response", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://localhost:8080", nil)
		if err != nil {
			t.Fatal(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal(err)
		}
		defer resp.Body.Close()

		time.Sleep(210 * time.Millisecond)
		cancel()
		data, err := io.ReadAll(resp.Body)
		if err != nil && !errors.Is(err, context.Canceled) {
			t.Fatal(err)
		}

		if !bytes.Equal(data, expected) {
			t.Error(string(data))
		}
	})

	t.Run("test with client", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://localhost:8080", nil)
		if err != nil {
			t.Fatal(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal(err)
		}
		defer resp.Body.Close()

		ch, errCh := sse.ScanData(ctx, resp.Body)

		var wg sync.WaitGroup
		wg.Add(2)
		var msgs []string
		go func() {
			for msg := range ch {
				msgs = append(msgs, string(msg))
			}
			wg.Done()
		}()
		go func() {
			for err := range errCh {
				t.Error(err)
			}
			wg.Done()
		}()

		time.Sleep(210 * time.Millisecond)
		cancel()
		wg.Wait()

		if !slices.Equal(msgs, []string{"hello\nworld", "hello\nworld", "hello\nworld"}) {
			t.Errorf("%q", msgs)
		}
	})
}
